from django.urls import path, include
from rest_framework import routers
from rest_framework_nested import routers
from . import views

router = routers.DefaultRouter()
router.register('chanels', views.ChanelViewSet, basename='chanels')
posts_router = routers.NestedDefaultRouter(
    router, 'chanels', lookup='chanel')
posts_router.register('series', views.SerieViewSet, basename='series')

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(posts_router.urls)),
]
