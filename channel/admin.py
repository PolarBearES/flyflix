from django.contrib import admin

# Register your models here.
from .models import Chanel, Serie, Chapter, Group


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    pass

@admin.register(Chanel)
class ChanelAdmin(admin.ModelAdmin):
    list_display = ('__str__','slug')
    list_filter = ['groups__title']
    search_fields = ['@title', '@description', '@serie__title', 'groups__title']
    prepopulated_fields = {
        'slug': ['title']
    }

class ChapterInline(admin.TabularInline):
    model = Chapter

@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'slug','chanel')
    list_filter = ['chanel']
    search_fields = ['@title', '@description']
    inlines = [ChapterInline]
    prepopulated_fields = {
        'slug': ['title']
    }

