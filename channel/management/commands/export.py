import os

import pandas
from django.core.management.base import BaseCommand
from django.db import connection

from flyflix.settings import BASE_DIR


class Command(BaseCommand):
    help = 'Calculate the ratings of every channel and export them in a csv file sorted by rating'

    def handle(self, *args, **options):
        print('Fetching the database...')
        sql = '''   With RECURSIVE recusive_query (rating) AS (
                        SELECT avg(cs.rating) rating,cc.id,cc.title,cc.parent_id FROM channel_serie cs
                                        INNER JOIN channel_chanel cc on cc.id = cs.chanel_id
                                        group by cc.id, cc.title
                        union
                        select rating,c.id,c.title,c.parent_id FROM recusive_query rq inner join channel_chanel c on c.id = rq.parent_id
                    )
                    SELECT round(avg(rating),2) rating,title FROM recusive_query GROUP BY title,id order by rating desc;'''

        with connection.cursor() as cursor:
            cursor.execute(sql)
            channels_rating = cursor.fetchall()
        pandas.DataFrame(channels_rating,columns=['Rating','Title']).to_csv(path_or_buf=os.path.join(BASE_DIR, 'export.csv'),sep=";",index=False)
        print('Done')