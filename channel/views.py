from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.viewsets import ModelViewSet

from .models import Chanel, Serie
from .serializers import ChanelSerializer, ChanelSerializerDetail, SerieSerializerDetail, SerieSerializer


class ChanelViewSet(ModelViewSet):
    @method_decorator(cache_page(60 * 60 * 2))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)
    @method_decorator(cache_page(60 * 60 * 2))
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)
    lookup_field = 'slug'
    filter_backends = [DjangoFilterBackend, SearchFilter]
    search_fields = ['@title', '@description', '@serie__title', 'groups__title']
    filter_fields = ['groups']

    def get_queryset(self):
        if self.action == 'retrieve':
            return Chanel.objects.prefetch_related('groups').all()
        return Chanel.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ChanelSerializerDetail
        return ChanelSerializer


class SerieViewSet(ModelViewSet):
    @method_decorator(cache_page(60 * 60 * 2))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)
    @method_decorator(cache_page(60 * 60 * 2))
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)
    lookup_field = 'slug'
    filter_backends = [SearchFilter]
    search_fields = ['@title', '@description']

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return SerieSerializerDetail
        return SerieSerializer

    def get_serializer_context(self):
        return {'chanel_slug': self.kwargs['chanel_slug']}
    def get_queryset(self):
        if self.action == 'retrieve':
            return Serie.objects.filter(chanel__slug=self.kwargs['chanel_slug']).prefetch_related('chapter_set').all()
        return Serie.objects.filter(chanel__slug=self.kwargs['chanel_slug']).all()
