from django.db.models.fields import related_lookups
from rest_framework import serializers

from .models import Chanel, Group, Serie, Chapter


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        # fields = ['']
        fields = '__all__'


class ChanelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chanel
        fields = ['slug', 'title', 'description', 'parent']


class ChanelSerializerDetail(ChanelSerializer):
    groups = GroupSerializer(many=True, read_only=True)

    class Meta(ChanelSerializer.Meta):
        fields = ['id', 'title', 'description', 'parent', 'groups']


class ChapterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chapter
        fields = ['title','episode','season','file']


class SerieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Serie
        fields = ['title', 'slug', 'description','rating']


class SerieSerializerDetail(SerieSerializer):
    chapter_set = ChapterSerializer(many=True, read_only=True)

    class Meta(SerieSerializer.Meta):
        fields = ['title', 'slug', 'description','rating', 'chapter_set']
