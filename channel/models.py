from decimal import Decimal

from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator, FileExtensionValidator
from django.db import models


class Group(models.Model):
    title: str = models.CharField(max_length=225)
    description: str = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.title


class Chanel(models.Model):
    title: str = models.CharField(max_length=225)
    slug = models.SlugField(max_length=255, unique=True)
    description = RichTextUploadingField(blank=True, null=True)
    parent = models.ForeignKey("self", on_delete=models.PROTECT, blank=True, null=True)
    groups = models.ManyToManyField(Group)

    def __str__(self):
        return self.title


class Serie(models.Model):
    title: str = models.CharField(max_length=225)
    slug = models.SlugField(max_length=255, unique=True)
    description = RichTextUploadingField(blank=True, null=True)
    rating: Decimal = models.DecimalField(max_digits=4, decimal_places=2,
                                          validators=[MaxValueValidator(10), MinValueValidator(0)])
    chanel = models.ForeignKey(Chanel, on_delete=models.PROTECT)

    def __str__(self):
        return self.title


class Chapter(models.Model):
    title: str = models.CharField(max_length=225, blank=True, null=True)
    file = models.FileField(upload_to='movies', validators=[FileExtensionValidator(allowed_extensions=['mp4'])])
    episode: int = models.PositiveIntegerField(blank=True, null=True)
    season: int = models.PositiveIntegerField(blank=True, null=True)
    serie = models.ForeignKey(Serie, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.serie} - S{self.season}E{self.episode} - {self.title}'
